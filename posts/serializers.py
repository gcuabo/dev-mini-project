from rest_framework import serializers

from .models import Post
from users.serializers import UserSerializer
from threads.serializers import ThreadSerializer

class PostSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    thread = ThreadSerializer()

    class Meta:
        model = Post
        fields = (
            'id',
            'thread',
            'user',
            'date_posted', 
            'post', 
        )
