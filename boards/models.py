from django.db import models

from app.mixins.modelMixin import ModelMixin, PrimaryKeyMixin


class Topic(ModelMixin, PrimaryKeyMixin):
    name = models.CharField(max_length=200)


class Board(ModelMixin, PrimaryKeyMixin):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    thread_count = models.IntegerField(default=0)
    post_count = models.IntegerField(default=0)
    order = models.IntegerField(null=True, default=0)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, blank=True, null=True)
