from django.contrib.auth import authenticate
from rest_framework import viewsets, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK, 
    HTTP_201_CREATED
)
from django.db.models.base import ObjectDoesNotExist

from .models import (
    User,
    UserFollower
)
from .serializers import (
    UserSerializer,
    UserFollowerSerializer
)
from posts.models import Post
from user_profiles.models import UserProfile
from posts.serializers import PostSerializer


# TODO Move this to Auth
class RegisterView(APIView):
    """
    Register a user

    * No Authenticaion needed
    """

    def post(self, request, *args, **kwargs): 
        """
        post a user object
        """
        data = request.data
        email = data['email']
        phone_number = data['phone_number']
        password = data['password']
        username = (data['username']) if 'username' in data else None

        if not email or not phone_number or not password: 
            return Response(status=HTTP_400_BAD_REQUEST)
        
        user = User.objects.create(
            email=email,
            phone_number=phone_number,
            username=username
        )

        user.set_password(password)
        user.save()

        return Response(user.to_json(), status=HTTP_201_CREATED)


# TODO Move this to Auth
class LoginView(APIView):
    """
    Login a user
    and return
    """

    def post(self, request, *args, **kwargs): 
        """
        post a user object
        """
        data = request.data
        email = data['email'] if 'email' in data else None
        phone_number = data['phone_number'] if 'phone_number' in data else None
        password = data['password']

        if (not email and not phone_number) or not password: 
            return Response(status=HTTP_400_BAD_REQUEST)
        
        if email: 
            username = email
        else: 
            username = phone_number

        user = authenticate(username=username, password=password)
        
        if not user: 
            return Response(status=HTTP_404_NOT_FOUND)
        
        token, _ = Token.objects.get_or_create(user=user)

        return Response({'token': token.key}, status=HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    def update(self, request, pk=None):
        data = request.data['profile']
        
        # Optional params
        fb = data['facebook'] if 'facebook' in data else None
        website = data['website'] if 'website' in data else None
        gender = data['gender'] if 'gender' in data else None
        interests = data['interests'] if 'interests' in data else None

        user = User.objects.get(id=pk)
        try: 
            profile = UserProfile.objects.get(id=user.profile_id)
            
            profile.about = data['about']
            profile.date_of_birth = data['date_of_birth']
            profile.hometown = data['hometown']
            profile.present_location = data['present_location']
            profile.geo_location = data['geo_location']
            profile.facebook = fb
            profile.website = website
            profile.gender = gender
            profile.interests = interests
            profile.save()
            
        except ObjectDoesNotExist: 
            profile = UserProfile.objects.create(
                about=data['about'],
                date_of_birth=data['date_of_birth'],
                hometown=data['hometown'],
                present_location=data['present_location'],
                geo_location=data['geo_location'],
                facebook=fb,
                website=website,
                gender=gender,
                interests=interests
            )
            profile.save()
            user.profile = profile
        
        serializer = self.get_serializer(user)
        return Response(serializer.data)


class UserFollowerViewSet(viewsets.ModelViewSet):
    """
    Follow/Unfollow a user
    """
    queryset = UserFollower.objects.all()
    serializer_class = UserFollowerSerializer
    permission_classes = [permissions.IsAuthenticated]
    filterset_fields = ['user_id']

    # Get user Followers
    def list(self, request, user_id=None):
        if not user_id: 
            user_id = request.user.id
        followers = UserFollower.objects.filter(user_id=user_id)
        serializer = self.get_serializer(followers, many=True)
        return Response(serializer.data)

    # follow user
    def create(self, request, user_id=None): 
        if not user_id: 
            user_id = request.user.id

        data = request.data

        if user_id == data['follower_id']: 
            return Response(status=HTTP_400_BAD_REQUEST)

        user_follower = UserFollower.objects.create(
            user_id=user_id,
            follower_id=data['follower_id']
        )
        serializer = self.get_serializer(user_follower)
        return Response(serializer.data, status=HTTP_201_CREATED)

    # unfollow user
    def delete(self, request, user_id=None): 
        if not user_id: 
            user_id = request.user.id

        try:
            UserFollower.objects.filter(
                user_id=user_id, 
                follower_id=request.data['follower_id']
            ).delete()
        except:
            pass
        return Response(status=HTTP_204_NO_CONTENT)


class UserPostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticated]
    filterset_fields = ['user_id']

    # Get user posts
    def list(self, request, user_id=None):
        if not user_id: 
            user_id = request.user.id
        posts = Post.objects.filter(user_id=user_id)
        serializer = self.get_serializer(posts, many=True)
        return Response(serializer.data)



class UserBanStatusViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    # ban user
    def create(self, request, user_id): 
        if (request.user.id == user_id): 
            return Response(status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(id=user_id)
        user.banned = True

        serializer = self.get_serializer(user)
        return Response(serializer.data)

    # set to sticky to false on post
    def delete(self, request, user_id): 
        user = User.objects.get(id=user_id)
        user.banned = False

        serializer = self.get_serializer(user)
        return Response(serializer.data)