from rest_framework import serializers

from user_profiles.serializers import UserProfileSerializer
from .models import User, UserFollower

class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'phone_number',
            'username',
            'date_joined',
            'profile'
        )


class UserFollowerSerializer(serializers.ModelSerializer):
    follower = UserSerializer()

    class Meta:
        model = UserFollower
        fields = (
            'follower',
        )
