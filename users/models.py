from django.db import models
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractBaseUser
)

from app.mixins.modelMixin import ModelMixin
from user_profiles.models import UserProfile


class UserManager(BaseUserManager):
    def create_user(self, email, password):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        
        user = self.model(
            email=self.normalize_email(email),
            username=self.normalize_email(email),
        )
    
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password=password
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, ModelMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    phone_number = models.CharField(
        max_length=30, 
        unique=True,
        verbose_name='phone number',
        null=True
    )
    username = models.CharField(
        max_length=30,
        unique=True,
        null=True)
    confirmed = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False) # a admin user; non super-user
    admin = models.BooleanField(default=False) # a superuser
    date_joined = models.DateTimeField(null=True)
    banned = models.BooleanField(default=False)
    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [] # Email & Password are required by default.

    objects = UserManager()
    
    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active

    @property
    def is_banned(self):
        "Is the user banned?"
        return self.banned

    def to_json(self): 
        return {
            "id": self.id, 
            "email": self.email, 
            "phone_number": self.phone_number, 
            "username": self.username, 
            "banned": self.is_banned, 
            "active": self.is_active 
        }


# TODO Move this to new file
class UserFollower(ModelMixin):
    class Meta:
        unique_together = (('user', 'follower'),)

    user = models.ForeignKey(User, related_name='user', on_delete=models.DO_NOTHING)
    follower = models.ForeignKey(User, related_name='follower', on_delete=models.DO_NOTHING)


# TODO MOVE THIS
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .tasks import send_verification_email

@receiver(post_save, sender=User)
def after_save(sender, instance, created, **kwargs):
    if created: 
        # Create token for user
        token = Token.objects.create(user=instance)
        # Send verification email
        send_verification_email.delay(instance.pk)