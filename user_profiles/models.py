from django.conf import settings
from django.db import models

from app.mixins.modelMixin import ModelMixin


class UserProfile(ModelMixin):
    about = models.TextField()
    date_of_birth = models.DateField()
    hometown = models.CharField(max_length=200)
    present_location = models.CharField(max_length=200)
    geo_location = models.CharField(max_length=200)
    facebook = models.CharField(max_length=200, null=True)
    website = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=50, null=True)
    interests = models.CharField(max_length=200, null=True)

