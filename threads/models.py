from django.db import models

from app.mixins.modelMixin import ModelMixin, PrimaryKeyMixin

from boards.models import Board
from users.models import User


class Thread(ModelMixin, PrimaryKeyMixin):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    last_user_posted = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    sticky = models.BooleanField(default=False)
    thread = models.TextField()
