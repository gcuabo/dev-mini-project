from django.db import models


class PrimaryKeyMixin(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)


class ModelMixin(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
