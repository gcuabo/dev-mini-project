"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import include, path
from rest_framework import routers
from django.conf.urls.static import static

import users.views as userView
import threads.views as threadViews
import boards.views as boardViews

# TODO declutter routes
router = routers.DefaultRouter()
router.register(r'users', userView.UserViewSet)
router.register(r'users/(?P<user_id>\d+)/follows', userView.UserFollowerViewSet)
router.register(r'users/(?P<user_id>\d+)/banstatus', userView.UserBanStatusViewSet)
router.register(r'users/(?P<user_id>\d+)/posts', userView.UserPostViewSet)
router.register(r'topics', boardViews.TopicViewSet)
router.register(r'boards', boardViews.BoardViewSet)
router.register(r'boards/(?P<board_id>\d+)/threads', threadViews.ThreadViewSet)
router.register(r'threads/(?P<thread_id>\d+)/stickystatus', threadViews.ThreadStickyStatusViewSet)
router.register(r'threads/(?P<thread_id>\d+)/posts', threadViews.ThreadPostViewSet)
router.register(r'threads', threadViews.ThreadViewSet)
router.register(r'me/follows', userView.UserFollowerViewSet)
router.register(r'me/posts', userView.UserPostViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    # register endpoint
    path('api/register', userView.RegisterView.as_view()),  
    path('api/login', userView.LoginView.as_view()),

    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
